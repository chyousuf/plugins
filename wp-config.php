<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'newplugin' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XBPi}|7Xm~vf?(IV]+3=`709M_cq%|_Ek<<>w_Uui=PH8D|y}1uK+![/AjeRYj;(' );
define( 'SECURE_AUTH_KEY',  'WcNN!2K?_Pn5gCX&y22n]NC=8Q@n~-PqRo8eZ9VWw[UL@J:-2(vxPm0ytmbPfvAL' );
define( 'LOGGED_IN_KEY',    '1gv/VY*PCGGBX}Q^y4B-E0A@Co&E=*Q5Db+!_GB+xe,7tN^kw_IEIk||ke24 GwN' );
define( 'NONCE_KEY',        ' yv~=Egol_&6P$^3fDKH}OTm`UZ7?0CBdD[!(]$]og6b]{Yx|swz`Y98/1oP+O^`' );
define( 'AUTH_SALT',        ',/XQdWUK9iE=]eX-,/<Y7Ym{|73t@r[)b/y>zc!9fD?MuhUfn0A7?pDmA&;mYqHp' );
define( 'SECURE_AUTH_SALT', '.O%c1y.s;#us,?@Bu^_p=ujA_@!j3f|Ow(8xl[(?z)S;yFu3oU2rB.b=9,O{`axu' );
define( 'LOGGED_IN_SALT',   'qP:[~ht<z;$Rnsl3P]_PwUrK)Nz[SU0 74RA{m>[Q`WSnbI,)LD,7sJhx|!u.u$4' );
define( 'NONCE_SALT',       'G)<~V[`~l>7bq~O}kZS`N0q#n;,ePFm9l H/j/o)EwUN2o%,+YhrXcbd`WMn0bp{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
